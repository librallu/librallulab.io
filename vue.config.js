
module.exports = {
    pages: {
        'index': {
            entry: './src/pages/Home/main.js',
            template: 'public/index.html',
            title: 'Home',
            chunks: [ 'chunk-vendors', 'chunk-common', 'index' ]
        },
        'teaching': {
            entry: './src/pages/Teaching/main.js',
            template: 'public/index.html',
            title: 'Teaching',
            chunks: [ 'chunk-vendors', 'chunk-common', 'teaching' ]
        },
        'research': {
            entry: './src/pages/Research/main.js',
            template: 'public/index.html',
            title: 'Research',
            chunks: [ 'chunk-vendors', 'chunk-common', 'research' ]
        },
        'projects': {
            entry: './src/pages/Projects/main.js',
            template: 'public/index.html',
            title: 'Projects',
            chunks: [ 'chunk-vendors', 'chunk-common', 'projects' ]
        },
        'awards': {
            entry: './src/pages/Awards/main.js',
            template: 'public/index.html',
            title: 'Awards',
            chunks: [ 'chunk-vendors', 'chunk-common', 'awards' ]
        },
    }
  }