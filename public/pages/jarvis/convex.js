
let root = d3.select("#demo-jarvis svg");
let width = 550;
let height = 200;

// define points of the polygon
let points_data = [];
let convex_hull = [];

let points_layer = root.append('g');
let annotation_layer = root.append('g');
let annotations_list = [];
let anim_time = 500;

function updatePoints(points_data) {
  let circles = points_layer.selectAll("circle")
    .data(points_data);

  circles.enter()
    .append("circle")
    .attr('class', 'point')
    .attr("cx", function(d) {return d.x;})
    .attr("cy", function(d) {return d.y;})
    .attr("r", 3);

  circles.exit().remove();
}


updatePoints(points_data);

root.on('click', function() {
  var coords = d3.mouse(this);
  points_data.push({x:coords[0], y:coords[1]});
  updatePoints(points_data);
});

function right_turn(a,b,c) {
  let ab_x = points_data[b].x-points_data[a].x;
  let ab_y = points_data[b].y-points_data[a].y;
  let bc_x = points_data[c].x-points_data[b].x;
  let bc_y = points_data[c].y-points_data[b].y;
  return ab_x*bc_y-ab_y*bc_x >= 0;
}

function random_unif(min,max) {
  return Math.random()*(max-min)+min;
}

function animation_point(point_id, color, name, time) {
  annotations_list.push({
    action:function() {
      annotation_layer.append('circle')
        .attr('cx', points_data[point_id].x)
        .attr('cy', points_data[point_id].y)
        .attr('r', 5)
        .attr('fill', color)
        .attr('class', name)
      ;
    },
    time: time
  });
}

function animation_remove(name, time) {
  annotations_list.push({
    action: function() {
      annotation_layer.selectAll('.'+name).remove();
    }, time:time
  });
}

function animation_half_space(a,b,color,name,time) {
  annotations_list.push({
    action:function() {
      let x1 = points_data[a].x;
      let x2 = points_data[b].x;
      let y1 = points_data[a].y;
      let y2 = points_data[b].y;
      let s = (y2-y1)/(x2-x1);
      let xa = 0;
      let ya = y1-x1*s;
      let xb = width;
      let yb = y2+s*(width-x2);
      let path = "";
      if ( x2-x1 > 0 && yb >= height ) { // three points
        path = `${xa},${ya},${xb},${yb},${-5},${height+5}`;
      } else if ( x2-x1 > 0 && yb < height ) {
        path = `${xa},${ya},${xb},${yb},${width+5},${height+5},${-5},${height+5}`;
      } else if ( x2-x1 < 0 && yb <= 0 ) {
        path = `${xa},${ya},${xb},${yb},${-5},${-5}`;
      } else if ( x2-x1 < 0 && yb > 0 ){
        path = `${xa},${ya},${xb},${yb},${width+5},${-5},${-5},${-5}`;
      }
      //console.log('('+xa+','+ya+')'+' -> '+'('+xb+','+yb+')')
      //annotation_layer.selectAll('.halfspace').remove();
      annotation_layer.append('polyline')
        .style('stroke', color)
        .style('stroke-width', '1')
        .style('fill', color)
        .style('fill-opacity', '0.1')
        .attr("points", path)
        .attr('class', name)
      ;
    }, time:time
  });
}

function animation_hull(N, time) {
  annotations_list.push({
    action: function() {
      annotation_layer.selectAll(".angle").remove();
      annotation_layer.selectAll(".hull").remove();
      for ( let i = 0 ; i < N-1 ; i++ ) {
        let a = i;
        let b = (i+1)%convex_hull.length;
        annotation_layer.append("line")
          .style("stroke", "black")
          .style("stroke-width", 2)
          .attr("class", "hull")
          .attr("x1", points_data[convex_hull[a]].x)
          .attr("x2", points_data[convex_hull[b]].x)
          .attr("y1", points_data[convex_hull[a]].y)
          .attr("y2", points_data[convex_hull[b]].y);
      }
    }, time: time
  });
}

function compute_convex_hull() {
  let N = points_data.length;

  // before computing, start putting some randomization on points to
  // remove degeneracies induced when two points have same coordinates
  let epsilon = 0.001;
  for ( let i = 0 ; i < N ; i++) {
    points_data[i].x += random_unif(-epsilon,epsilon);
    points_data[i].y += random_unif(-epsilon,epsilon);
  }

  if ( N == 0 ) return [];
  if ( N == 1) return [0];

  // step 1 find point with the smaller x coordinate (p_start)
  let p_start_id = 0;
  for ( let i = 1 ; i < N ; i++ ) {
    let px = points_data[p_start_id].x;
    let py = points_data[p_start_id].y;
    let ax = points_data[i].x;
    let ay = points_data[i].y;
    if ( ax < px || (ax==px&&ay<py) ) {
      p_start_id = i;
    }
  }
  if ( N == 2) return [p_start_id, 1-p_start_id];
  convex_hull = [];

  // step 2 add a point to a convex hull.
  // repeat until the last point added is the same as p_start
  let annotation_first_pass = true;
  let h = 0;
  let q_now_id = p_start_id;
  let q_next_id = 1;

  do {

    convex_hull.push(q_now_id);
    (function(N) {
      animation_hull(N, anim_time);
    })(convex_hull.length);

    // ANIMATION display p_start and q_now_id
    (function(a,b) {
      animation_remove("red", 0);
      animation_remove("blue", 0);
      animation_remove("half", 0);
      if ( a != b )
        animation_half_space(a, b, "red", "half", 0);
      animation_point(a, "red", "red", 0);
      animation_point(q_next_id, "blue", "blue", anim_time);
    })(q_now_id, q_next_id);

    // find next point in convex hull
    for ( let i = 0 ; i < N ; i++ ) {
      if ( i != q_now_id && i != q_next_id ) {
        // console.log("turn("+q_now_id+","+q_next_id+","+i+")");
        (function(i) {
          animation_remove("green",0);
          animation_point(i,"green","green", anim_time);
        })(i);
        if ( right_turn(q_now_id, q_next_id, i) ) {
          // console.log("q_next:"+q_next_id+" -> "+i);
          q_next_id = i;
          (function(a,b) {
            animation_remove("red", 0);
            animation_remove("blue", 0);
            animation_remove("half", 0);
            animation_half_space(a, b, "red", "half", 0);
            animation_point(a, "red", "red", 0);
            animation_point(q_next_id, "blue", "blue", anim_time);
          })(q_now_id, q_next_id);
        }
      }
    }
    (function(a,b) {
      animation_remove("green",0);
      animation_remove("half",0);
      animation_half_space(a,b, "gray", "half_end", 0);
    })(q_now_id, q_next_id);
    q_now_id = q_next_id;
    q_next_id = p_start_id;
    // console.log({q_now_id:q_now_id,p_start:p_start_id});
  } while(q_now_id != p_start_id);

  return convex_hull;
}

function execute_animations(step_list) {
  if ( step_list.length > 0 ) {
    let step = step_list.shift();
    step.action();
    setTimeout(function() {
      execute_animations(step_list);
    }, step.time);
  }
}

function launch_demo_jarvis() {
  root.selectAll(".hull").remove();
  annotation_layer.selectAll('*').remove();
  let hull = compute_convex_hull();
  console.log(hull);

  // ANIMATION: display hull
  animation_hull(hull.length+1, anim_time);
  // ANIMATION: remove temp elements
  animation_remove("red", 0);
  animation_remove("blue", 0);
  animation_remove("half", 0);

  // Display all animations
  execute_animations(annotations_list);

}

function reset_demo_jarvis() {
  // remove points entered
  points_data = [];
  updatePoints(points_data);
  root.selectAll(".hull").remove();
  annotation_layer.selectAll('*').remove();

}

document.querySelector("#button_launch_jarvis").onclick = launch_demo_jarvis;
document.querySelector("#button_reset_jarvis").onclick = reset_demo_jarvis;
