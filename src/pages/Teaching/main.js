import Vue from 'vue'
import Teaching from './Teaching.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(Teaching),
}).$mount('#app')
