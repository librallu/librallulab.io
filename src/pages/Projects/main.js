import Vue from 'vue'
import Projects from './Projects.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(Projects),
}).$mount('#app')
