import Vue from 'vue'
import Awards from './Awards.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(Awards),
}).$mount('#app')
