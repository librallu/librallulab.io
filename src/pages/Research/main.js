import Vue from 'vue'
import Research from './Research.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(Research),
}).$mount('#app')
